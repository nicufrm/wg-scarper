'use strict';

var express = require('express');
var fs = require('fs');
var request = require('request');
var requestPromise = require('request-promise');
var cheerio = require('cheerio');
var app     = express();

let hasCaptcha = (html) => html.includes("In the event of a breach of the special conditions")

let parseHtml = (html) => {
    let $ = cheerio.load(html);
    let supertrim = (t) => {
        return t.replace(/\s\s/g, " ").replace(/\s\s/g, " ").replace(/\s\s/g, " ").replace(/\s\s/g, " ").trim()
                .replace(/\s\s/g, " ").replace(/\s\s/g, " ").replace(/\s\s/g, " ").replace(/\s\s/g, " ").trim()
                .replace(/\s\s/g, " ").replace(/\s\s/g, " ").replace(/\s\s/g, " ").replace(/\s\s/g, " ").trim()
                .replace(/\s\s/g, " ").replace(/\s\s/g, " ").replace(/\s\s/g, " ").replace(/\s\s/g, " ").trim()
    }

    let images = [];
    $('img.sp-image.img-responsive.center-block').each((i,e)=>images.push($(e).attr('data-large')))

    var info = {
        id: $('.col-md-8.text-right').text().trim(),
        title: $('#sliderTopTitle').text().trim(),
        type: $('#sliderTopTitle span').attr('title'),
        size: $($('h2.headline.headline-key-facts')[0]).text().trim(),
        price: $($('h2.headline.headline-key-facts')[1]).text().trim(),
        dates: supertrim($('div.panel.panel-default div.panel-body div[class=row] div.col-sm-3 ').text()),
        address: supertrim($('a[onclick="$(\'#map_tab\').click();"]').text()),
        images: images,
    }

    return info;
}

let initCookies = () => {
    return new Promise(function(resolve, reject){
        requestPromise({url: "https://www.wg-gesucht.de/en/wg-zimmer-in-Berlin-Neukoelln.6579118.html", jar:true}).then(function(html){
            resolve()
        })
    })
}
let parsePage = (url) => {
    return new Promise((resolve, reject) => {
        requestPromise({url: url, jar:true}).then(function(html){
            let isBlockedByCaptcha = hasCaptcha(html);
            console.log ("URL: ", url, " captha: ", isBlockedByCaptcha)
            let parsedData = parseHtml(html);
            resolve({url, parsedData, html, isBlockedByCaptcha});
        });
    });
}

// let parsePages = (urls) => urls.map(parsePage)

let sleep = (ms) => {
    return new Promise((res,rej) => {
        setTimeout(() => res(), ms)
    })
}

let parseSearchPage = (url) => {
    return new Promise((resolve, reject) => {
        requestPromise({url: url, jar:true}).then(function(html){
            console.log ("URL: ", url, " captha: ", hasCaptcha(html))
            let $ = cheerio.load(html);
            let adUrls = []
            $('.headline.headline-list-view.noprint.truncate_title .detailansicht').each((i,e) =>
                adUrls.push($(e).attr('href'))
            )
            resolve(adUrls);
        })
    })
}

let getAdPages = (callback) => {
    let baseUrl = "https://www.wg-gesucht.de/en/"
    let startUrl = "https://www.wg-gesucht.de/en/wg-zimmer-in-Berlin.8.0.1.0.html?offer_filter=1&noDeact=1&city_id=8&category=0&rent_type=0&rMax=500"
    let pageUrl = (page) => "https://www.wg-gesucht.de/en/wg-zimmer-in-Berlin.8.0.1."+page+".html"
    let startPage = 0
    let endPage = 5 //281
    let adPages = [];


    requestPromise({url: startUrl, jar:true}).then(async function(html){
        for(let page=startPage; page<=endPage; page++){
            let adUrls = await parseSearchPage(pageUrl(page))
            let x = await sleep(2000);
            adPages = adPages.concat(adUrls.map(url => baseUrl + url))
        }
        console.log(JSON.stringify(adPages,null, " "), " Ads:", adPages.length);

        const filename = "output/output-data-" + (new Date).toISOString().replace(':',"-").replace(':',"-").replace(':',"-").replace('.',"-") + ".json";

        fs.writeFile(filename, "[\n", "utf8", ()=>{})

        for(let adIndex = 0; adIndex < adPages.length; adIndex++){
            let adData = await parsePage(adPages[adIndex])
            console.log("Parsed ad ", adIndex+1, " of ", adPages.length)

            if (adData.isBlockedByCaptcha) break;
            adData.index = adIndex
            const pageFilname = "output/pages/adpage-data-" + (new Date).toISOString().replace(':',"-").replace(':',"-").replace(':',"-").replace('.',"-") + "-index-" + adIndex + ".html";
            fs.writeFile(pageFilname,adData.html, "utf8", ()=>{})
            delete adData.html;
            fs.appendFile(filename,JSON.stringify(adData) + ",\n", "utf8", ()=>{})

            let x = await sleep(10000);
        }

        fs.appendFile(filename, "]\n", "utf8", ()=>{})

        callback(html);
    })
}



app.get('/scrape', function(req, res){

  initCookies().then(()=>{

      getAdPages((h) => res.send(h))

  })

})

app.listen('8081')

console.log('Magic happens on port 8081');

exports = module.exports = app;
